import thunk from 'redux-thunk';
import { applyMiddleware, createStore } from 'redux';
import logger from 'redux-logger';
import reducers from '../reducer/rootReducer';

export const store = createStore(reducers, applyMiddleware(thunk, logger))
