import { combineReducers } from 'redux'
import { articleReducer } from './articleReducer';
import { authorReducer } from './authorReducer';


const reducers = combineReducers({
    articleReducer,
    authorReducer
});

export default reducers;
