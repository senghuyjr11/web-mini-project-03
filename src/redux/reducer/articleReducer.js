const initialState = {
    articles: [],
}

export const articleReducer = (state = initialState, { type, payload }) => {
    switch (type) {

    case "FETCH_ARTICLE":
        return { ...state, articles: payload }

    default:
        return state
    }
}
