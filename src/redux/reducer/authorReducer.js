import {authorActionType} from '../actions/authorActionType'
const initialState = {
    authors: [],
}

export const authorReducer = (state = initialState, { type, payload }) => {
    switch (type) {

    case "FETCH_AUTHOR":
        return { ...state, authors: payload }

    case authorActionType.DELETE_AUTHOR: {
            let authors = state.authors.filter(authors => authors.id !== payload)
            return { ...state, authors: [...authors] }
        }
    default:
        return state
    }
}
