import axios from 'axios';

const api = axios.create({
    baseURL: 'http://110.74.194.124:3034/api',
});

//fetch article by id
export const fetchArticleById = async (id) => {
    try {
      const result = await api.get(`/articles?page=1&size=2/${id}`)
      console.log("fetchArticleById:", result.data.data);
      return result.data.data
    } catch (error) {
      console.log("fetchArticleById error:", error);
    }
  }

  //fetch all article
export const fetchAllArticle = async () => {
    try {
        const result = await api.get("/articles?page=1&size=2")
        console.log("result articles", result.data.data);
        return result.data.data;
    } catch (error) {
        console.log("error articles", error);
    }
}


export const addArticle = async (articles) => {
  try {
    const result = await api.post("/articles?page=1&size=2", articles);
    console.log("addArticle:", result.data.data);
  } catch (error) {
    console.log("addArticle Error:", error);
  }
}

  //upload image to api
  export const uploadImage = async (image) => {
    try {
      const fd = new FormData();
      fd.append("image", image)
      const result = await api.post("/images", fd, {
        onUploadProgress: progress => {
          console.log("Uploaded:", Math.floor(progress.loaded / progress.total * 100));
        }
      })
      console.log("uploadImage:", result.data.url);
      return result.data.url
    } catch (error) {
      console.log("uploadImage Error:", error);
    }
  }

    //update author by ID
export const updateArticleById = async (id, article) => {
  try {
    const result = await api.put(`/articles?page=1&size=2/${id}`, article)
    console.log("updateAuthorById:", result.data.data);
    alert(result.data.message)
    return result.data.data
  } catch (error) {
    console.log("updateAuthorById Error:", error);
  }
}

export default api;