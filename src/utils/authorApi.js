import axios from 'axios';

//Base URL for Author
export const API = axios.create({
    baseURL: "http://110.74.194.124:3034/api"
})

export const addAuthor = async (author) => {
  try {
    const result = await API.post("/author", author);
    console.log("addtutorials:", result.data.data);
    return result.data.data;
  } catch (error) {
    console.log("addAuthor Error:", error);
  }
}

//fetch all Author
export const fetchAllAuthor = async () => {
    try {
        const result = await API.get("/author")
        console.log("result author", result.data.data);
        return result.data.data;
    } catch (error) {
        console.log("error author", error);
    }
}


//fetch Author by id
export const fetchAuthorById = async (id) => {
    try {
        const result = await API.get(`/author/${id}`)
        console.log("fetchAuthorById:", result.data.data);
        return result.data.data
    } catch (error) {
        console.log("author error:", error);
    }
}

//delete Author by ID
export const deleteAuthorById = async (id) => {
    try {
      const result = await API.delete(`/author/${id}`);
      console.log("deleteAuthorById:", result.data.data);
      alert(result.data.message)
      return result.data.data;
    } catch (error) {
      console.log("deleteAuthorById Error:", error);
    }
  }

  //upload image to api
  export const uploadImage = async (image) => {
    try {
      const fd = new FormData();
      fd.append("image", image)
      const result = await API.post("/images", fd, {
        onUploadProgress: progress => {
          console.log("Uploaded:", Math.floor(progress.loaded / progress.total * 100));
        }
      })
      console.log("uploadImage:", result.data.url);
      return result.data.url
    } catch (error) {
      console.log("uploadImage Error:", error);
    }
  }


//Add author to api
export const addTutorials = async (author) => {
  try {
    const result = await API.post("/tutorials", author);
    console.log("addtutorials:", result.data.data);
  } catch (error) {
    console.log("addAuthor Error:", error);
  }
}


//   //update author by ID
// export const updateAuthorById = async (id, author) => {
//     try {
//       const result = await API.put(`/author/${id}`, author)
//       console.log("updateAuthorById:", result.data.data);
//       alert(result.data.message)
//       return result.data.data
//     } catch (error) {
//       console.log("updateAuthorById Error:", error);
//     }
//   }