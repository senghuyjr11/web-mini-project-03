import "./App.css";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import { Container, Row } from "react-bootstrap";
import Home from "./pages/Home";
import MenuBar from "./components/MenuBar";
import AritcleAdd from "./pages/AritcleAdd";
import View from "./pages/article/View";
import Author from "./pages/author/Author";

function App() {
  return (
    <>
      <BrowserRouter>
        <MenuBar />
        <Container>
          <Switch>
            <Route exact path="/">
              <Row style={{ marginTop: 20 }}> <Home /></Row>
            </Route>
            <Route path="/aritcleadd" component={AritcleAdd}/>
            <Route path="/author" component={Author}/>
            <Route path="/view/:id" component={View} />
          </Switch>
        </Container>
      </BrowserRouter>
    </>
  );
}

export default App;
