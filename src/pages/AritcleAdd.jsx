import React, { useState } from 'react';
import { BrowserRouter as Router, useHistory, useLocation, useRouteMatch } from "react-router-dom";
import query from 'query-string';
import { Form, Button, Row, Col, Image } from 'react-bootstrap';
import { addArticle, fetchAllArticle, updateArticleById, uploadImage } from '../utils/api';
import { BoxLoading } from 'react-loadingg';

export default function AritcleAdd() {
    const [author, setAuthor] = useState([]);
    const [myImage, setMyImage] = useState(null);
    const [browsedImage, setBrowsedImage] = useState("");

    const [article, setArticle] = useState([]);
    const history = useHistory();
    const [title, setTitle] = useState("");
    const [name, setName] = useState("");
    const [description, setDescriptioin] = useState("");

    const placeholder = "https://media.sproutsocial.com/uploads/2017/02/10x-featured-social-media-image-size.png";

    const { search } = useLocation()
    console.log("Location:", search);

    let { id } = query.parse(search)
    console.log("id query:", id);

    //Browse image from and add to image tag
    function onBrowsedImage(e) {
        setMyImage(e.target.files[0]);
        setBrowsedImage(URL.createObjectURL(e.target.files[0]));
    }

    //Add/Update onAddAuthor to api
    async function onAddArticle() {
        if (search === "") {
            const url = myImage && await uploadImage(myImage);
            const article = {
                title,
                name,
                description,
                image: url ? url : placeholder,
            };
            await addArticle(article);
        } else {
            const url = myImage && await uploadImage(myImage);
            const article = {
                title,
                name,
                description,
                image: url ? url : placeholder,
            };
            await updateArticleById(id, article)
        }
        const result = await fetchAllArticle();
        console.log("Author:", result);
        setArticle(result);
        setName("")
        setDescriptioin("")
        setBrowsedImage(placeholder)
        setMyImage("")
        history.push("")
    }

    return (
        <div>
            <Router>
                <Row>
                    <Col md={8}>
                        <Form>
                            <h2>Author</h2>
                            <Form.Group className="mb-3" controlId="formBasicPassword">
                                <Form.Label>Article</Form.Label>
                                <Form.Control value={title} onChange={(e) => setTitle(e.target.value)} type="text" placeholder="Title" />
                            </Form.Group>

                            <div className="form-group">
                                <select className="mx-2 custom-select">
                                    {
                                        article.map((item, index) => {
                                            return (
                                                <option key={item._id} value={item.title} >{item.title}</option>
                                            );
                                        })
                                    }

                                </select>
                            </div>

                            <Form.Group className="mb-3" controlId="formBasicEmail">
                                <Form.Label>Author</Form.Label>
                                <Form.Control
                                    value={description} onChange={(e) => setDescriptioin(e.target.value)} type="text" placeholder="Author" />
                            </Form.Group>
                            <Button onClick={onAddArticle} variant="primary">{search ? "Update" : "Add"}</Button>
                        </Form>
                    </Col>
                    <Col md={4}>
                        <div style={{ width: "300px" }}>
                            <label htmlFor="myfile">
                                <Image width="100%" src={browsedImage ? browsedImage : placeholder} />
                            </label>
                        </div><br />
                        <input
                            onChange={onBrowsedImage}
                            id="myfile"
                            type="file"
                            style={{ display: "" }}
                        />
                    </Col>
                </Row>
            </Router>
        </div>
    )
}
